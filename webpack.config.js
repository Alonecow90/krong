module.exports = {
  mode: 'development',
  entry: './public/js/game/index.js',
  output: {
    filename: 'bundle.js',
    path: __dirname + '/public/js'
  },
}
