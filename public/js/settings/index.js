export const batSettings = {
  y: 0,
  w: 25,
  h: 75,
  offset: 10,
}

export const ballSettings = {
  w: 25,
  offset: 4,
  speed: 2,
  maxSpeed: 4,
}

export const maxPoint = 10
