import { canvas, ctx, clearScreen } from '../common/canvas'
import socket from '../common/socket'
import { maxPoint } from '../settings'
import Bat from './Bat'
import Ball from './Ball'

const players = [
  new Bat('left'),
  new Bat('right')
]

let controls = {}

let ball = null

let execute = null

let points = [0, 0]

socket.on('player-ready', num => {

  if (num < players.length) {

    controls = {

      'ArrowUp': {
        status: 0,
        callback: () => {
          players[num].up()
          socket.emit('bat-move', { num, dir: -1 })
        }
      },

      'ArrowDown': {
        status: 0,
        callback: () => {
          players[num].down()
          socket.emit('bat-move', { num, dir: 1 })
        }
      },

    }

    execute = waiting

  }

})

socket.on('game-ready', vector => {

    ball = new Ball(vector)

    execute = gameplay

})

socket.on('bat-move', ({ num, dir }) => {

  players[num].update(dir)

})

socket.on('ball-reset', vector => {

  ball = new Ball(vector)

  execute = gameplay

})

const isIntersectBat = (ball, bat) => {

  return !(
    ball.x + ball.w <= bat.x || ball.y + ball.w <= bat.y
    || ball.x > bat.x + bat.w || ball.y >= bat.y + bat.h
  )

}

const isIntersectScreen = () => {

  if (ball.x + ball.w > canvas.width) {

    return 0

  }

  else if (ball.x < 0) {

    return 1

  }

  return false

}

const gameplay = () => {

  Object.values(controls).map( v => {

    if (v.status) {

      v.callback()

    }

  })

  if (ball) {

    players.map(player => {

      if (isIntersectBat(ball, player)) {

        let x = (ball.dirX < 0) ? player.w : player.x - ball.w 

        ball.changeDirection(x)

        ball.speedUp()

      }

    })

    ball.update()

    ball.draw()

    let goal = isIntersectScreen()

    if (goal !== false) {

      points[goal] += 1

      ball = null

      if (points[goal] < maxPoint) {

        execute = showScore

        socket.emit('goal')

      }

      else {

        execute = gameOver(goal + 1)

      }

    }

  }

  players.map(player => {

    player.draw()

  })

}

const waiting = () => {

  let text = 'Waiting player 2...'

  let { width } = ctx.measureText(text)

  ctx.fillText(text, (canvas.width / 2 - width / 2), canvas.height / 2)

}

const showScore = () => {

  gameplay()

  ctx.save()

  ctx.font = '100px Pixel Emulator'

  const width = canvas.width / 2

  points.map( (point, k) => {

    const offset = 150

    const { width:textWidth } = ctx.measureText(point)

    const x = (k === 0) ? (width - offset - textWidth) : width + offset

    const y = offset

    ctx.fillText(point, x, y)

  })

  ctx.restore()

}

const gameOver = (player) => () => {

  const gameOverText = 'Game over'

  const winText = `Player ${player} win`

  const { width:gowidth } = ctx.measureText(gameOverText)

  const { width:winwidth } = ctx.measureText(winText)

  ctx.fillText(winText, (canvas.width / 2 - winwidth / 2), canvas.height / 2)

  ctx.fillText(gameOverText, (canvas.width / 2 - gowidth / 2), canvas.height / 3)

}

const loop = () => {

  clearScreen()

  if (execute) {

    execute()

  }

  requestAnimationFrame(loop)

}

loop()

document.addEventListener('keydown', ({ key }) => {

  if (controls[key]) {

    controls[key].status = 1

  }

})

document.addEventListener('keyup', ({ key }) => {

  if (controls[key]) {

    controls[key].status = 0

  }

})
