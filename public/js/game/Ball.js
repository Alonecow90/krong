import { canvas, ctx } from '../common/canvas'
import { ballSettings as initSettings } from '../settings'

class Ball {
  
  constructor( [x, y] ) {

    this.dirX = x

    this.dirY = y

    this.init()

  }

  init() {

    Object.entries(initSettings).forEach( ([k, v]) => {

      this[k] = v

    })

    this.x = (canvas.width / 2) - (this.w / 2)

    this.y = (canvas.height / 2) - (this.w / 2)

  }

  check() {

    if (this.y < 0) {

      this.y = 0

      this.dirY *= -1

    }

    else if (this.y > canvas.height - this.w) {

      this.y = canvas.height - this.w

      this.dirY *= -1

    }

  }

  update() {

    this.x += this.dirX * this.offset * this.speed

    this.y += this.dirY * this.offset * this.speed

    this.check()

  }

  speedUp() {

    if (this.speed < this.maxSpeed) {

      this.speed += 0.3

    }

  }

  changeDirection(x) {

    this.x = x

    this.dirX *= -1

  }

  draw() {

    ctx.fillRect(this.x, this.y, this.w, this.w)

  }

}

export default Ball
