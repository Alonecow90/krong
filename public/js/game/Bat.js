import { canvas, ctx } from '../common/canvas'
import { batSettings as initSettings } from '../settings'

class Bat {

  constructor(side) {

    this.x = (side === 'right') ? (canvas.width - initSettings.w) : 0

    this.init()

  }

  init() {

    Object.entries(initSettings).forEach( ([k, v]) => {

      this[k] = v

    })

  }

  check() {

    if (this.y < 0) {

      this.y = 0

    }

    else if (this.y > canvas.height - this.h) {

      this.y = canvas.height - this.h

    }

  }

  update(dir) {

    this.y += this.offset * dir

    this.check()

  }

  up() {

    this.y -= this.offset

    this.check()

  }

  down() {

    this.y += this.offset

    this.check()
    
  }

  draw() {

    ctx.fillRect(this.x, this.y, this.w, this.h)

  }

}

export default Bat
