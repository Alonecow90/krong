export const canvas = document.getElementById('canvas')

export const ctx = canvas.getContext('2d')

ctx.fillStyle = 'white'

ctx.font = '50px Pixel Emulator'

export const clearScreen = () => {

  ctx.clearRect(0, 0, canvas.width, canvas.height)

}
