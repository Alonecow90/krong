import io from 'socket.io-client'

let socket = io('http://192.168.1.43:8080')

const testConnection = socket => {

  window.addEventListener('load', () => socket.emit('client-ping', 'Client ready'))

  socket.on('server-pong', msg => console.log(msg))

}

testConnection(socket)

export default socket
