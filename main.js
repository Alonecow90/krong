let http = require('http')
let fs = require('fs')
let path = require('path')
let io = require('socket.io')
let timer = 3000

const server = http.createServer((req, res) => {

  const fsHandler = (err, data) => {

    if (err) {

      res.writeHead(500)

      return res.end('File loading error')

    }

    const contentType = (path.extname(req.url) === '.css') ? 'text/css' : ''

    res.writeHead(200, { 'Content-Type' : contentType })

    res.end(data)

  }

  if (req.url === '/') {

    fs.readFile(__dirname + '/public/index.html', (err, data) => fsHandler(err, data))

  }

  else {

    fs.readFile(__dirname + `/public${req.url}`, (err, data) => fsHandler(err, data))

  }

})

server.listen(8080, () => console.log('Listening 192.168.1.43:8080'))

const getRandom = () => {

  return ( (Math.random() - 0.5) > 0 ) ? 0.5 : -0.5

}

let socket = io(server)

socket.on('connection', sct => {

  sct.on('client-ping', msg => {

    console.log(msg)

    sct.emit('server-pong', 'Server ready')

  })

  sct.on('bat-move', ({ num, dir }) => {

    sct.broadcast.emit('bat-move', { num, dir })

  })

  const connetionsCount = Object.values(socket.sockets.connected).length

  sct.emit('player-ready', connetionsCount - 1)

  if (connetionsCount === 2) {

    console.log('Game ready')

    socket.emit('game-ready', [getRandom(), getRandom()])

  }

  sct.on('goal', () => {

    setTimeout(() => {

      socket.emit('ball-reset', [getRandom(), getRandom()])

    }, timer)

  })

})
